# Docker GitFlow C.I./C.D. Tools

The purpose of this project is to abstract away C.I./C.D. config from the C.I/C.D. machine/tool you're running on (such as travis, GitLab, or Visual Studio Team Services)

The ultimate goal is to be able to have an easy configuration in the repo of your project that doesn't require a bunch of tool-knowledge / trial and error. *coughvstsistheworstatthiscough*

## Usage

```bash
# download gitflow-ci to your scripts/ci directory
./scripts/ci/gitflow-ci
```

## The Process

These environment variables are provided to each script
 - CURRENT_BRANCH
 - IS_MASTER
 - IS_DEVELOP
 - IS_RELEASE
 - UNBUILT_COMMIT_EXISTS
 - COMMIT_TO_BUILD

### Build

### Quality

### Test

### Tag

- A Docker Login script may be needed to login to the registry
  - this will be run automatically if it's named `docker-login.sh` in the scripts folder.

### Deploy


## Configuration

### Dependencies

 - docker-compose
 - a distributable package of this repo (no need to install ruby)

### Required ENV vars

 - DOCKER_REGISTRY_URL
   - e.g.: registry.example.com/
 - DOCKER_REGISTRY_NAME
   - e.g.: project-name

### Optional ENV vars

 - DOCKER_CUSTOM_TAG


## Using in your C.I./C.D. tool of choice

### GitLab

```yaml
image: tmaier/docker-compose:latest

run:
  script: rake run
```

### VSTS

### Locally

```bash
rake run
```

or pre-packaged via:

## Initial Setup

### Scripts for your project

```
- projectRoot/
  - scripts/
    - ci/
      - build.sh
      - test.sh
      - quality.sh
      - deploy.sh
      - clean.sh
```
This default setup will be picked up automatically.  
Make sure to use `set -e` in your scripts if multiple commands are run.
These scripts should return exit status of 0 for success.


## Development

```bash
# build initial local docker image
rake run
# test success with
./run.sh
```
