// #![cfg_attr(feature="clippy", feature(plugin))]
// #![cfg_attr(feature="clippy", plugin(clippy))]

extern crate subprocess;
extern crate regex;
extern crate glob;
extern crate yaml_rust;

mod gitflow_ci;
use gitflow_ci::{ shell, git, build };

fn main() {
    info();
    build();

    println!("{}", shell::run("echo $CURRENT_BRANCH"));

    println!("Done!")
}

fn info() {
    let pwd = shell::run("pwd");
    let output = git::current_branch();
    let sha = git::current_commit();
    println!("\n");
    print!(" ===========================================\n");
    print!("||         GitFlow CI Helper Tools         ||\n");
    print!(" ===========================================\n");
    println!("pwd: {}", pwd);
    print!("current commit: {}", sha);
    print!("current branch: {}", output);
    println!();
}

fn build() {
    if gitflow_ci::git::is_master() {
        return build::master();
    }

    if gitflow_ci::git::is_develop() {
        return build::develop();
    }

    if gitflow_ci::git::is_release() {
        return build::release();
    }


    build::other()
}
