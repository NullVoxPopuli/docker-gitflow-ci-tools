pub use gitflow_ci::shell;

use regex::Regex;
use glob::Pattern;

/// # Examples
///
/// GitFlow Branch names should match the pattern ^([-_\/\w]*)$:
///
/// ```rust,should_panic
/// assert!(current_branch());
/// assert_eq!(current_branch(), "develop");
/// ```
pub fn current_branch() -> String {
    let branches = &*shell::run("git branch");
    parse_git_branch_for_current(branches)
}

fn parse_git_branch_for_current(branches: &str) -> String {
    let regex = Regex::new(r"\* (.+)").unwrap();
    let captures = regex.captures(branches).unwrap();
    let result = captures.get(1).map_or("", |m| m.as_str());

    result.to_string()
}

pub fn current_commit() -> String {
    shell::run("git rev-parse HEAD")
}

pub fn last_non_merge_commit() -> String {
    // @last_non_merge_commit ||= begin
    //   sha = nil
    //
    //   `git log --pretty=format:"%H"`.split("\n").each do |lsha|
    //     sha = lsha
    //     break unless merge_commit?(sha)
    //   end
    //
    //   sha
    // end
    "".to_string()
}

pub fn does_unbuilt_commit_exist() -> bool {
    // develop_branch = DockerGitFlowCI.configuration.develop_branch
    // commit_diff = `git cherry -v #{develop_branch} | grep "^\+"`
    //
    // !commit_diff.split("\n").empty?
    let commit_diff = commit_diffs_against_branch("develop");
    let diffs = commit_diff.split("\n");

    diffs.count() > 0
}

fn commit_diffs_against_branch(branch: &str) -> String {
    let command = format!("git cherry -v {} | grep \"^\\+\"", branch);
    shell::run(&*command)
}

pub fn is_master() -> bool {
    current_branch() == "master"
}

pub fn is_develop() -> bool {
    current_branch() == "develop"
}

pub fn is_release() -> bool {
    does_match_release_pattern(&*current_branch())
}

fn does_match_release_pattern(branch: &str) -> bool {
    Pattern::new("release/*").unwrap().matches(branch)
}

pub fn is_merge_commit(sha: &str) -> bool {
    let msha = merge_commit_matching(sha);
    sha == &*msha
}

fn merge_commit_matching(sha: &str) -> String {
    let command = format!("git rev-list --merges {}-1..{}", sha, sha);
    shell::run(&*command)
}


#[cfg(test)]
mod tests {
    use gitflow_ci::git;
    use regex::Regex;

    #[test]
    fn test_does_match_release_pattern() {
        assert_eq!(true, git::does_match_release_pattern("release/1.0"));
        assert_eq!(false, git::does_match_release_pattern("release"));
        assert_eq!(true, git::does_match_release_pattern("release/the-great-release-of-2011"));
    }

    #[test]
    fn test_current_branch() {
        let branch = git::parse_git_branch_for_current("* develop\nmaster");
        let reg = Regex::new(r"^([-\w_/]*)").unwrap();
        let captures = reg.captures(&*branch).unwrap();
        let result = captures.get(1).map_or("", |m| m.as_str());

        assert_eq!("develop", result);
    }
}
