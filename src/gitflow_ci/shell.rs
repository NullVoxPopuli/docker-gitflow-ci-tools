use subprocess::Exec;

pub fn run(script: &str) -> String {
    Exec::shell(script)
        .capture().unwrap().stdout_str()
}
