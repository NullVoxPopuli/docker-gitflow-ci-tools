pub use gitflow_ci::shell;

use std::path::Path;

pub fn build() {
    info("Starting Build...");
    run("build.sh")
}
pub fn quality() {
    info("Running Quality Checks...");
    run("quality.sh")
}
pub fn test() {
    info("Running Tests...");
    run("test.sh")
}
pub fn tag() {
    info("Tagging...");
    run("tag.sh")
}
pub fn deploy() {
    info("Beginning Deploy...");
    run("deploy.sh")
}
pub fn clean() {
    info("Cleaning Up...")
}

fn info(msg: &str) {
    println!("{}", msg);
}

fn run(name: &str) {
    let file_path = ["./scripts/ci/", name].concat();
    let file_reference = &*file_path;
    let script = Path::new(file_reference);

    if script.exists() {
        shell::run(file_reference);
        return;
    }

    println!("{} not found. Skipping...", file_path);

}
