pub use gitflow_ci::git;
pub use gitflow_ci::script;

use std::env;

pub fn master() {
    prepare_env();
    script::deploy();
}

pub fn develop() {
    prepare_env();
    script::build();
    script::tag();
    script::quality();
    script::test();
    script::deploy();
    clean();
}

pub fn release() {
    prepare_env();
    if git::does_unbuilt_commit_exist() {
        script::build();
        script::tag();
        script::quality();
        script::test();
        clean();
    }
}

pub fn other() {
    prepare_env();
    script::quality();
    script::test();
}

fn clean() {
    script::clean();
}

fn prepare_env() {
    env::set_var("CURRENT_BRANCH",
        git::current_branch());

    env::set_var("IS_MASTER",
        bool_to_string(
            git::is_master()));

    env::set_var("IS_DEVELOP",
        bool_to_string(
            git::is_develop()));

    env::set_var("IS_RELEASE",
        bool_to_string(
            git::is_release()));

    env::set_var("UNBUILT_COMMIT_EXISTS",
        bool_to_string(
            git::does_unbuilt_commit_exist()));

    env::set_var("COMMIT_TO_BUILD",
        git::last_non_merge_commit());

}

fn bool_to_string(condition: bool) -> String {
    let mut result = "false";

    if condition {
        result = "true"
    }

    result.to_string()
}
