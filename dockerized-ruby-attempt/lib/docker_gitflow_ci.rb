# frozen_string_literal: true

require 'yaml'
require 'pathname'
require 'colorize'

require_relative 'docker_gitflow_ci/configuration'
require_relative 'docker_gitflow_ci/build'
require_relative 'docker_gitflow_ci/git'
require_relative 'docker_gitflow_ci/version'

# Namespace for this gem
module DockerGitFlowCI
  class << self
    attr_accessor :configuration
  end

  # TODO: import env vars via .env
  module_function

  def call
    return Build.master if DockerGitFlowCI::Git.master?
    return Build.develop if DockerGitFlowCI::Git.develop?
    return Build.release if DockerGitFlowCI::Git.release?

    Build.other
  rescue => e
    display_info
    raise e
  end

  def info(msg)
    puts ''
    puts ''
    puts ''
    puts msg.colorize(:yellow)
  end

  def display_info
    puts '------------ DEBUG INFO --------------'
    puts 'Working Directory '
    puts "\t" + `pwd`
    puts ''
    puts `uname -a`
    puts '--------------------------------------'
  end

  def configuration
    @configuration ||= DockerGitFlowCI::Configuration.new
  end

  def config
    configuration
  end

  def configure
    yield(configuration)
  end
end
