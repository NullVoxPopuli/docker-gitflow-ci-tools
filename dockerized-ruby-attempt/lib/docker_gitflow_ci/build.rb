# frozen_string_literal: true
require_relative './script'

module DockerGitFlowCI
  module Build
    module_function

    def master
      prepare_env
      Script.deploy
    end

    def develop
      prepare_env
      Script.build
      Script.tag
      Script.quality
      Script.test
      Script.deploy
      clean
    end

    def release
      prepare_env
      if Git.unbuilt_commit_exists?
        Script.build
        Script.tag
        Script.quality
        Script.test
        clean
      end

      Script.deploy
    end

    def other
      prepare_env
      Script.quality
      Script.test
      clean
    end

    def clean
      Script.clean
    end

    def prepare_env
      git = DockerGitFlowCI::Git

      ENV['CURRENT_BRANCH'] = git.current_branch
      ENV['IS_MASTER'] = git.master?.to_s
      ENV['IS_DEVELOP'] = git.develop?.to_s
      ENV['IS_RELEASE'] = git.release?.to_s
      ENV['UNBUILT_COMMIT_EXISTS'] = git.unbuilt_commit_exists?.to_s
      ENV['COMMIT_TO_BUILD'] = git.last_non_merge_commit
    end
  end
end
