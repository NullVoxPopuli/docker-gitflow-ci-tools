# frozen_string_literal: true
module DockerGitFlowCI
  module Script
    module_function

    def build
      info 'Starting Build...'
      run(DockerGitFlowCI.config.build_script_path)
    end

    def quality
      info 'Running Quality Checks...'
      run(DockerGitFlowCI.config.quality_script_path)
    end

    def test
      info 'Running Tests...'
      run(DockerGitFlowCI.config.test_script_path)
    end

    def tag
      info 'Starting Tagging...'

      # Tag
      registry_url = ENV['DOCKER_REGISTRY_URL']
      registry_name = ENV['DOCKER_REGISTRY_NAME']

      # Registry URL should be part of the tagging process
      registry = "#{registry_url}/#{registry_name}"
      sha = DockerGitFlowCI::Git.last_non_merge_commit
      branch = DockerGitFlowCI::Git.current_branch

      execute(%(
        docker tag #{registry_name}:#{branch} #{registry}:#{sha}
        docker tag #{registry_name}:#{branch} #{registry}:latest
        docker tag #{registry_name}:#{branch} #{registry_name}:latest

        #if [ -n $DOCKER_CUSTOM_TAG ]; then
        #  docker tag #{registry_name}:#{branch} #{registry}:$DOCKER_CUSTOM_TAG
        #fi
      ))
    end

    def info(msg)
      DockerGitFlowCI.info(msg)
    end

    def deploy
      info 'Beginning Deploy...'
      run(DockerGitFlowCI.config.deploy_script_path)
    end

    def clean
      info 'Cleaning Up...'
      run(DockerGitFlowCI.config.clean_script_path)
    end

    def run(script_path)
      path = prepend_context(script_path)
      execute(path)
    end

    def prepend_context(script)
      DockerGitFlowCI.config.script_context + script
    end

    def execute(script)
      system(script) || raise("#{script} did not exit with status 0")
    end
  end
end
