# frozen_string_literal: true
module DockerGitFlowCI
  # Contains all the configurable options
  class Configuration
    attr_accessor :docker_compose_file,

      :develop_branch,
      :master_branch,
      :release_pattern,

      :deploy_to_test_env,

      :script_context,
      :build_script_path,
      :quality_script_path,
      :test_script_path,
      :deploy_script_path,
      :clean_script_path,

      :tag_git_on_release_merge

    DEFAULTS = {
      docker_compose_file:  'docker-compose.ci.yml',

      develop_branch:  'develop',
      master_branch:  'master',
      release_pattern: 'release/*',

      deploy_to_test_env:  false,

      script_context:  './scripts/ci/',
      build_script_path:  'build.sh',
      quality_script_path:  'quality.sh',
      test_script_path:  'test.sh',
      deploy_script_path:  'deploy.sh',
      clean_script_path: 'clean.sh'
    }.freeze

    VALID_CONFIG_KEYS = DEFAULTS.keys

    def initialize(path_to_yaml_file = 'ci.yml')
      read_configuration(path_to_yaml_file)
    end

    # Configure through hash
    def apply_config(hash = {})
      hash.each do |k, v|
        option_name = k.to_sym
        next unless VALID_CONFIG_KEYS.include?(option_name)

        instance_variable_set("@#{option_name}", v)
      end
    end

    # Configure through yaml file
    def read_configuration(path_to_yaml_file)
      config_exists = Pathname.new(path_to_yaml_file).exist?

      return apply_config(DEFAULTS) unless config_exists

      config = YAML.safe_load(IO.read(path_to_yaml_file))
      apply_config(config)
    rescue Errno::ENOENT
      log(:warning, "YAML configuration file couldn't be found.")
    rescue Psych::SyntaxError
      log(:warning, 'YAML configuration file contains invalid syntax.')
    end
  end
end
