# frozen_string_literal: true
module DockerGitFlowCI
  module Git
    module_function

    def run(script)
      DockerGitFlowCI::Script.execute(script)
    end

    # git symbolic-ref HEAD | sed 's!refs\/heads\/!!'
    def current_branch
      @current_branch ||= begin
        `git branch`.split("\n")
                    .map(&:strip)
                    .select { |branch| branch.start_with?('*') }
                    .first
                    .gsub('* ', '')
      end
    end

    # git rev-parse HEAD
    def current_commit
      @current_commit ||= `git rev-parse HEAD`.strip
    end

    # latestNonMergeCommitHash() {
    #    lastTenCommits=$(git rev-list HEAD~10..HEAD)
    #
    #   for x in $lastTenCommits; do
    #      isMerge=$(isMergeCommit $x)
    #
    #     if [ ! $isMerge ]; then
    #       echo "$x";
    #       return 0;
    #     fi
    #   done
    #
    #   echo "NO VALID COMMIT FOUND"
    #   exit 1
    # }
    #
    # isMergeCommit() {
    #     sha=$1
    #     msha=$(git rev-list --merges ${sha}~1..$sha)
    #     [ -z "$msha" ] && return 1
    #     return 0
    # }
    #
    # hash=$(latestNonMergeCommitHash)
    def last_non_merge_commit
      @last_non_merge_commit ||= begin
        sha = nil

        `git log --pretty=format:"%H"`.split("\n").each do |lsha|
          sha = lsha
          break unless merge_commit?(sha)
        end

        sha
      end
    end

    def merge_commit?(sha)
      msha = `git rev-list --merges #{sha}~1..#{sha}`
      sha == msha.strip
    end

    def master?
      current_branch == DockerGitFlowCI.configuration.master_branch
    end

    def develop?
      current_branch == DockerGitFlowCI.configuration.develop_branch
    end

    def release?
      pattern = DockerGitFlowCI.configuration.release_pattern
      File.fnmatch(pattern, current_branch)
    end

    # This is needed because
    # release branches may
    # have commits are not present
    # in develop.
    # e.g.: the scenario where we have
    #       a hotfix -- branch off master
    #       or an existing release branch.
    def unbuilt_commit_exists?
      # look for commits that exists on the current branch
      # but don't exist in develop
      develop_branch = DockerGitFlowCI.configuration.develop_branch
      commit_diff = `git cherry -v #{develop_branch} | grep "^\+"`

      !commit_diff.split("\n").empty?
    end
  end
end
