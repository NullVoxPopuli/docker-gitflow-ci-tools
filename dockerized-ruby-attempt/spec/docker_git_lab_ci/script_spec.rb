# frozen_string_literal: true
require 'spec_helper'

klass = DockerGitFlowCI::Script
describe klass do
  describe '.execute' do
    it 'raises an exception upon non-zero exit status' do
      script = 'bash -c "exit 1"'
      expect { klass.execute(script) }
        .to raise_error(RuntimeError)
    end

    it 'does not raise an exception upon success' do
      script = 'bash -c "exit 0"'
      expect { klass.execute(script) }
        .to_not raise_error
    end
  end

  describe '.run' do
    it 'executes the script' do
      expect { klass.run('deploy.sh') }
        .to_not raise_error
    end
  end

  describe '.prepend_context' do
    it 'prepends the default context' do
      expect(klass.prepend_context('build.sh'))
        .to eq './scripts/ci/build.sh'
    end
  end
end
