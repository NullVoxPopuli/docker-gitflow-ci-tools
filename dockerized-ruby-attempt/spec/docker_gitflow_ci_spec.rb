# frozen_string_literal: true
require 'spec_helper'

klass = DockerGitFlowCI
describe klass do
  describe '.call' do
    before(:each) do
      allow(klass::Script).to receive(:run)
    end

    after(:each) do
      # allow(klass::Git).to receive(:master?).and_call_original
    end

    context 'on the master branch' do
      before(:each) do
        allow(klass::Git).to receive(:current_branch) { 'master' }
      end

      it 'calls the master build' do
        expect(klass::Build).to receive(:master)

        klass.call
      end
    end

    context 'on the develop branch' do
      before(:each) do
        allow(klass::Git).to receive(:current_branch) { 'develop' }
      end

      it 'calls the develop build' do
        expect(klass::Build).to receive(:develop)

        klass.call
      end
    end

    context 'on a release branch' do
      it 'calls the release build' do
        allow(klass::Git).to receive(:current_branch) { 'release/v1.2.3' }
        expect(klass::Build).to receive(:release)

        klass.call
      end
    end

    context 'on a feature branch' do
      it 'calls the other build' do
        allow(klass::Git).to receive(:current_branch) { 'feature/do-the-thing' }
        expect(klass::Build).to receive(:other)

        klass.call
      end
    end

    context 'on a non-gitflow-named branch' do
      it 'calls the other build' do
        allow(klass::Git).to receive(:current_branch) { 'derpderp-no-convention' }
        expect(klass::Build).to receive(:other)

        klass.call
      end
    end
  end
end
