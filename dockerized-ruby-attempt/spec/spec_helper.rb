# frozen_string_literal: true
require 'simplecov'
SimpleCov.start

require 'docker_gitflow_ci'
