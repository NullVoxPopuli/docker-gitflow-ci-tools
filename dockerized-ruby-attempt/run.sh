#!/bin/sh
 docker run \
  --rm  \
  --volume "$PWD":/code  \
  --volume /usr/bin:/usr/bin \
  nullvoxpopuli/gitflow-ci rake run
