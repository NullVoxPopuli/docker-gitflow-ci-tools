# -*- encoding: utf-8 -*-
# frozen_string_literal: true

# allows bundler to use the gemspec for dependencies
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)

require 'docker_gitflow_ci/version'
name = DockerGitFlowCI.name
version = DockerGitFlowCI::VERSION

Gem::Specification.new do |s|
  s.name        = name
  s.version     = version
  s.platform    = Gem::Platform::RUBY
  s.license     = 'MIT'
  s.authors     = ['L. Preston Sego III']
  s.email       = 'LPSego3+dev@gmail.com'
  s.homepage    = 'https://gitlab.com/NullVoxPopuli/docker-gitflow-ci-tools'
  s.summary     = "#{name}-#{version}"
  s.description = 'Group related classes together. No more silos.'

  s.files        = Dir['LICENSE', 'README.md', 'lib/**/*']
  s.require_path = 'lib'

  s.test_files = s.files.grep(%r{^(test|spec|features)/})

  s.required_ruby_version = '>= 2.0'

  s.add_dependency 'rake'
  s.add_dependency 'colorize'

  # Quality Control
  s.add_development_dependency 'rubocop'
  s.add_development_dependency 'rspec'

  # Debugging
  s.add_development_dependency 'awesome_print'
  s.add_development_dependency 'pry-byebug'
end
